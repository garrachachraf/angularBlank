import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user.model';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor( private http: HttpClient , private router: Router) { }
  user: User ;
  errormsg: any;
  ngOnInit() {
  }

  checklogin(username: string , pass: string) {
    this.http.get<User>('http://localhost:18080/CsaRestAPI/users/' + username + '/' + pass).subscribe(res => {

      this.user = res ;
      //console.log(this.user);
        localStorage.setItem('currentUser', JSON.stringify(this.user));
        console.log(this.user.role);
        this.router.navigate(['/dashbord']);
    },
      error => {console.log(error);
      this.errormsg = 'Wrong Password';
    });
  }

}
